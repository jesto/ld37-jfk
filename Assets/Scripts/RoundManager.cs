﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts
{
	public class RoundManager
	{
		public IList<Round> Rounds = new List<Round>();

		public Round CurrentRound
		{
			get { return Rounds.FirstOrDefault(rnd => !rnd.IsCompleted()); }
		}

		public bool IsGameComplete()
		{
			return Rounds.All(rnd => rnd.IsCompleted());
		}
	}
}