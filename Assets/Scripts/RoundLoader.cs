﻿using Assets.Scripts;

public class RoundLoader
{
	public static void Load(RoundManager roundManager)
	{
		roundManager.Rounds.Add(new Round
		{
			RoundNumber = 1,
			Waves = new[]
			{
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_2_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_2_10"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_2_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_2_10"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_2_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"}
					}
				}
			}
		});
		roundManager.Rounds.Add(new Round
		{
			RoundNumber = 2,
			Waves = new[]
			{
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_2_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_14_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_0_5"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_2_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_0_5"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"}
					}
				}
			}
		});

		roundManager.Rounds.Add(new Round
		{
			RoundNumber = 3,
			Waves = new[]
			{
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_14_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_14_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"}
					}
				}
			}
		});

		roundManager.Rounds.Add(new Round
		{
			RoundNumber = 4,
			Waves = new[]
			{
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_14_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_14_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"}
					}
				}
			}
		});

		roundManager.Rounds.Add(new Round
		{
			RoundNumber = 5,
			Waves = new[]
			{
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_14_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_14_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"}
					}
				}
			}
		});

		roundManager.Rounds.Add(new Round
		{
			RoundNumber = 6,
			Waves = new[]
			{
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_14_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_14_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"}
					}
				},
				new Wave
				{
					Enemies = new[]
					{
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_0"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_13_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"},
						new EnemyDefinition {Health = 3, Type = EnemyType.Rat, SpawnTile = "tile_4_10"}
					}
				}
			}
		});
	}
}