﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
	public float _maxTimeInSeconds = 1.0f;
	private float _timeSinceCreation;

	// Use this for initialization
	void Enable ()
	{
		_timeSinceCreation = 0.0f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		_timeSinceCreation += Time.deltaTime;

		if (_timeSinceCreation >= _maxTimeInSeconds)
			Destroy(gameObject);
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Obstacle" || collider.tag == "Enemy")
		{
			Destroy(gameObject);
		}
	}
}
