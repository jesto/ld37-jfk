﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
	public GameObject Crosshair;
	public GameObject Bullet;
	public float WalkSpeed = 10;
	public TheRoom GameController { get; set; }

	private float _horizontalSpeed;
	private float _verticalSpeed;
	private Vector3 _mousePosition;
	private float _timeSinceLostShot;
	private Player Player;

	// Use this for initialization
	void Start ()
	{
		Crosshair = Instantiate(Crosshair, transform.position + transform.up, Quaternion.identity);
		Player = GetComponent<Player>();
	}

	void FixedUpdate()
	{
		var rb = GetComponent<Rigidbody2D>();
		rb.MovePosition(new Vector2(_horizontalSpeed + transform.position.x, _verticalSpeed + transform.position.y));
	}

	// Update is called once per frame
	void Update ()
	{
		if (Player.IsDead() || !GameController.IsGameRunning()) return;

		_horizontalSpeed = Input.GetAxis("Horizontal") != 0 ? Input.GetAxis("Horizontal") / 10 * WalkSpeed : 0;
		_verticalSpeed = Input.GetAxis("Vertical") != 0 ? Input.GetAxis("Vertical") / 10 * WalkSpeed : 0;
		_mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		_mousePosition.z = 0;

		Crosshair.transform.position = _mousePosition;
		_timeSinceLostShot += Time.deltaTime;

		if (Input.GetButton("Fire1") && _timeSinceLostShot >= 0.33)
		{
			var rotation = Mathf.Atan2(_mousePosition.y - transform.position.y, _mousePosition.x - transform.position.x) * Mathf.Rad2Deg;
			var bullet = Instantiate(Bullet, transform.position, Quaternion.Euler(0, 0, rotation));
			var rb = bullet.GetComponent<Rigidbody2D>();
			rb.AddForce(Vector3.ClampMagnitude(new Vector2(_mousePosition.x - transform.position.x, _mousePosition.y - transform.position.y), 1.0f) * 600);

			_timeSinceLostShot = 0;
		}
	}
}
