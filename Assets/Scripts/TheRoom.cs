﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TheRoom : MonoBehaviour
{
	public GameObject FloorTile;
	public GameObject NorthWall;
	public GameObject SouthWall;
	public GameObject EastWall;
	public GameObject WesthWall;
	public GameObject PlayerGameObject;
	public GameObject Enemy;
	public GameObject RoundText;
	public GameObject GameOverUI;
	public GameObject WinUI;
	
	private IList<GameObject> CurrentEnemies = new List<GameObject>(); 
	private RoundManager RoundManager = new RoundManager();

	// Use this for initialization
	void Start () {
		var width = 19;
		var height = 11;

		for (var x = 0; x < width; x++)
		{
			for (var y = 0; y < height; y++)
			{
				GameObject tileTemplate = FloorTile;

				var go = Instantiate(tileTemplate, new Vector3(x, y), Quaternion.identity, transform);
				go.name = string.Format("tile_{0}_{1}", x, y);
			}
		}

		PlayerGameObject = Instantiate(PlayerGameObject, new Vector3(4, 4), Quaternion.identity);
		Player = PlayerGameObject.GetComponent<Player>();
		Player.OnPlayerDeath += (player) =>
		{
			GameOverUI.SetActive(true);
			RoundText.SetActive(false);
			WinUI.SetActive(false);
		};
		PlayerGameObject.GetComponent<InputController>().GameController = this;

		GameOverUI.SetActive(false);
		WinUI.SetActive(false);
		RoundText.SetActive(true);

		LoadRounds();
	}

	public void Restart()
	{
		GameOverUI.SetActive(false);
		WinUI.SetActive(false);
		RoundText.SetActive(true);

		SceneManager.LoadScene("_SCENE_");
	}

	public void LoadRounds()
	{
		RoundLoader.Load(RoundManager);
	}

	private float _timeSinceLastSpawn;
	private float _timeTillNextRound = 3f;

	void FixedUpdate ()
	{
		_timeTillNextRound -= Time.deltaTime;
		
		if (_timeTillNextRound > 0)
		{
			RoundText.GetComponent<Text>().text = Mathf.Round(_timeTillNextRound + 0.5f).ToString();
		}
		else
		{
			SpawnNext();

			if ((_timeSinceLastSpawn += Time.deltaTime) > 0.25)
			{
				if (EnemiesToSpawn.Any())
				{
					var enemy = EnemiesToSpawn[0];
					SpawnEnemy(enemy);

					_timeSinceLastSpawn = 0;
					EnemiesToSpawn.RemoveAt(0);
				}
			}
		}
	}

	private IList<EnemyDefinition> EnemiesToSpawn = new List<EnemyDefinition>();
	private Player Player;
	private int currentRoundNumber = 0;

	private void SpawnNext()
	{
		if (CurrentEnemies.Any() || EnemiesToSpawn.Any())
			return;

		var round = RoundManager.CurrentRound;

		if (round == null)
			return;

		if (currentRoundNumber != round.RoundNumber)
		{
			UpdateRoundUI(round);

			currentRoundNumber = round.RoundNumber;
		}

		var wave = round.CurrentWave();

		if (wave == null)
			return;

		foreach (var enemyDef in wave.Enemies)
		{
			EnemiesToSpawn.Add(enemyDef);
		}
	}

	private void UpdateRoundUI(Round round)
	{
		RoundText.GetComponent<Text>().text = "Round " + round.RoundNumber;
		_TimeSinceLastRoundUIUpdate = 0;
	}

	private float _TimeSinceLastRoundUIUpdate;

	void Update()
	{
		_TimeSinceLastRoundUIUpdate += Time.deltaTime;
		
		var color = RoundText.GetComponent<Text>().color;
	  color.a = Mathf.Clamp01(-(_TimeSinceLastRoundUIUpdate / 4 -1 ));
		RoundText.GetComponent<Text>().color = color;
	}

	private void SpawnEnemy(EnemyDefinition enemyDef)
	{
		var enemy = Instantiate(Enemy, transform.FindChild(enemyDef.SpawnTile).transform.position, Quaternion.identity);
		var e = enemy.GetComponent<Enemy>();
		e.PlayerGameObject = PlayerGameObject;
		e.EnemyDefinition = enemyDef;
		e.OnEnemyKilled += enmy =>
		{
			CurrentEnemies.Remove(enmy);

			if (RoundManager.CurrentRound == null)
			{
				if (RoundManager.IsGameComplete())
				{
					WinUI.SetActive(true);
				}

				return;
			}

			var roundNumber = RoundManager.CurrentRound.RoundNumber;

			if (roundNumber != currentRoundNumber)
			{
				_timeTillNextRound = 3f;
				currentRoundNumber = roundNumber;
				_TimeSinceLastRoundUIUpdate = 0f;
			}
		};

		CurrentEnemies.Add(enemy);
	}

	public bool IsGameRunning()
	{
		return !RoundManager.IsGameComplete();
	}
}