﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;

public class Round
{
	public int RoundNumber { get; set; }
	public IList<Wave> Waves = new List<Wave>();

	public bool IsInProgress()
	{
		return Waves.SelectMany(wave => wave.Enemies).Any(enemy => enemy.IsAlive());
	}

	public bool IsCompleted()
	{
		return Waves.All(wave => wave.IsComplete());
	}

	public Wave CurrentWave()
	{
		return Waves.FirstOrDefault(wav => !wav.IsComplete());
	}
}

public class Wave
{
	public IList<EnemyDefinition> Enemies = new List<EnemyDefinition>();
	public string SpawnTile { get; set; }

	public bool IsComplete()
	{
		return Enemies.All(enemy => !enemy.IsAlive());
	}
}