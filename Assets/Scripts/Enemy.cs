﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public GameObject PlayerGameObject;
	public EnemyDefinition EnemyDefinition { get; set; }
	

	public float MoveSpeed = 5;
	public Action<GameObject> OnEnemyKilled;
	private Player Player;

	// Use this for initialization
	void Start () {
		Player = PlayerGameObject.GetComponent<Player>();
	}

	private float TimeSinceLastAttack = 0;

	void FixedUpdate()
	{
		TimeSinceLastAttack += Time.deltaTime;

		var distance = Vector3.Distance(transform.position, PlayerGameObject.transform.position);
		if (distance <= EnemyDefinition.AttackRange && TimeSinceLastAttack >= (1/ EnemyDefinition.AttacksPerSecond))
		{
			Player.TakeDamage(EnemyDefinition.Damage);

			TimeSinceLastAttack = 0;
		}

		if (distance > EnemyDefinition.AttackRange)
		{
			transform.Translate(Vector3.ClampMagnitude(PlayerGameObject.transform.position - transform.position, MoveSpeed / 100));
		}
	}

	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Bullet")
		{
			if (--EnemyDefinition.Health <= 0)
			{
				OnEnemyKilled(gameObject);

				Destroy(gameObject);
			}
		}
	}
}
