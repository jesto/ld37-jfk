﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
	public class EnemyDefinition
	{
		public EnemyType Type { get; set; }
		public int Health { get; set; }
		public string SpawnTile { get; set; }
		public int Damage = 1;
		public float AttackRange = 0.501f;
		public float AttacksPerSecond = 0.5f;

		public bool IsAlive()
		{
			return Health > 0;
		}
	}
}
