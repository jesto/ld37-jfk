﻿using System;
using UnityEngine;

public class Player : MonoBehaviour
{
	public int Health = 10;
	public Action<Player> OnPlayerDeath;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TakeDamage(int damage)
	{
		var isLeathalHit = Health - damage <= 0;
		if (isLeathalHit && IsDead() && OnPlayerDeath != null)
		{
			// play death animation
			OnPlayerDeath(this);
		}

		Health -= damage;
	}

	public bool IsDead()
	{
		return Health <= 0;
	}
}
